import {Component, Input, OnInit} from '@angular/core';
import { Topic } from '../model/topic';
import {TopicService} from '../services/topic/topic.service';
import {TimelineService} from '../services/timeline/timeline.service';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss']
})
export class TopicComponent implements OnInit {

  topics: Topic[] = [];

  @Input() timelineId: number;

  topic: Topic;

  constructor(private timelineService: TimelineService, private topicService: TopicService) {}

  async ngOnInit() {
    const timeline = await this.timelineService.fetch(this.timelineId);

    console.log(timeline.topics);

    for (const topic of timeline.topics) {
      this.topics.push(await this.topicService.fetch(topic as number));
    }
  }

  async remove(topic: Topic) {
    const timeline = await this.timelineService.fetch(this.timelineId);
    timeline.topics = timeline.topics.filter((t: number) => t !== topic.id);

    await this.timelineService.save(timeline);
    this.topics = this.topics.filter((t: Topic) => t.id !== topic.id);
  }
}
