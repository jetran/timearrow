import {Contribution} from './contribution';
import {Topic} from './topic';
import {Event} from './event';

export class Timeline {
    id: number;
    title: string;
    from: Date;
    to: Date;
    resume: string;
    topics: Array<number>;
    events: Array<number>;

    constructor(id = null, title = '', from = null, to = null, resume = '', topics = null, contributions= null, events = null) {
      this.id = id;
      this.title = title;
      this.from = from;
      this.to = to;
      this.resume = resume;
      this.topics = topics;
      this.events = events;
  }
}
