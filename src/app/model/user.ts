import {Contribution} from './contribution';

export class User {
  id: number;
  firstName: string;
  lastName: string;
  birthDate: Date;
  email: string;
  password: string;
  contributions: Contribution[];

  constructor(id, firstName, lastName, birthDate, email, password, contributions) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDate = birthDate;
    this.email = email;
    this.password = password;
    this.contributions = contributions;
  }
}
