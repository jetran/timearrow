export class Event {
  id: number;
  title: string;
  resume: string;
  image: string;
  video: string;
  content: string;
  from: Date = new Date();
  to: Date = new Date();
  timeline: number;

  constructor(id = null, title = null, resume = null, timeline = null, image = null, video = null, content = null) {
    this.id = id;
    this.title = title;
    this.resume = resume;
    this.image = image;
    this.video = video;
    this.content = content;
    this.timeline = timeline;
  }
}
