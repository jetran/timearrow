export class Contribution {

  constructor(public id: number = null,
              public eventBefore: number = null,
              public eventAfter: number = null,
              public timeline: number = null,
              public contribDate: Date = new Date(),
              public user: number = null,
              public state: number = 0) {
    this.id = id;
    this.eventBefore = eventBefore;
    this.eventAfter = eventAfter;
    this.timeline = timeline;
    this.contribDate = contribDate;
    this.user = user;
    this.state = 0;
  }
}
