import {Component, OnInit} from '@angular/core';
import {Timeline} from '../model/timeline';
import {FormControl} from '@angular/forms';
import {Topic} from '../model/topic';
import {TimelineService} from '../services/timeline/timeline.service';
import {MatDialog} from '@angular/material';
import {TopicService} from '../services/topic/topic.service';
import {TopicFormComponent} from '../topic-form/topic-form.component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-timeline-form',
  templateUrl: './timeline-form.component.html',
  styleUrls: ['./timeline-form.component.scss']
})
export class TimelineFormComponent implements OnInit {

  timeline: Timeline;
  topicsForm = new FormControl();
  topics = new Array<Topic>();
  newTopics = new Array<Topic>();

  constructor(
    private timelineService: TimelineService,
    private topicService: TopicService,
    public dialog: MatDialog,
    private router: Router) {
  }

  ngOnInit() {
    /* Topics repo mock */
    this.timeline = new Timeline();
    this.timeline.topics = [];
    this.queryTopics();
    this.topics.push(new Topic(null, 'brg', 'iojn', 'ojnojn'));
  }

  async submit() {
    const timeline = await this.timelineService.save(this.timeline);
    this.router.navigate(['/timeline', timeline.id]);
  }

  linkTopicToTimeline(topic: Topic) {

    if (!this.timeline.topics.includes(topic.id)) {
      this.timeline.topics.push(topic.id);
    } else {
      const index = this.timeline.topics.indexOf(topic.id);
      if (index > -1) {
        this.timeline.topics = this.timeline.topics.filter((t: number) => t !== topic.id);
      }
    }
  }

  async queryTopics() {
    this.topics = await this.topicService.fetchAll();
  }

  async openDialog() {
    const dialogRef = this.dialog.open(TopicFormComponent, {
      width: '250px',
      data: new Topic()
    });

    dialogRef.afterClosed().subscribe(async topic => {
      topic = await this.topicService.save(topic);
      this.topics.push(topic);
    });
  }


}
