import {Component, ElementRef, OnInit, Output, ViewChild } from '@angular/core';
import {Event} from '../model/event';
import {TimelineService} from '../services/timeline/timeline.service';
import {Timeline} from '../model/timeline';
import {ActivatedRoute} from '@angular/router';
import {EventService} from '../services/event/event.service';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {
  /* Configuration des options graphiques de la Timeline */
  alternate = true;
  toggle = true;
  color = false;
  expandEnabled = true;
  side = 'left';
  // TODO: dynamic size management
  size = 40;
  /* */

  /* sidebar */
  showFiller = false;
  expandedIndex = -1;
  editedIndex = -2;
  sideToggled = false;
  stopPropagation = false;
  repoEntries = [];
  offset = 0;
  timeline: Timeline;
  id: number;
  timelineEdit = false;

  @Output()
  editedEvent: Event;

  /* Passage du composant graphique de dessin de la sidebar */
  @ViewChild('drawer', null) drawer: any;

  /* Evènements de la Timeline */
  entries = [];

  constructor(
    private timelineService: TimelineService,
    private route: ActivatedRoute,
    public elementRef: ElementRef,
    private eventService: EventService) { }

  ngOnInit() {
    this.route.params.subscribe( params => this.id = params.id );
    /* Event repository mock */
    for (let i = 0; i < 200; i++) {
      this.repoEntries.push(new Event(0, i, '', '', '', ''));
    }
    this.queryTimeline();
  }

  async queryTimeline() {
     this.timeline = await this.timelineService.fetch(this.id);
     this.entries = await this.eventService.fetchByTimeline(this.timeline.id);
  }

  /* Fonction de tri des évènements par dates*/
  public get sortedArray(): Event[] {
    this.entries = this.entries.sort((a, b) => new Date(a.from).getTime() - new Date(b.from).getTime());
    return this.entries;
  }

  /* Trigger pour ouvrir la sidebar */
  openSideBar() {
    if (!this.drawer._opened) {
      this.drawer.toggle();
      this.sideToggled = true;
    }
  }

  /* Trigger pour fermer la sidebar */
  async closeSideBar() {
    if (this.drawer._opened) {
      this.drawer.toggle();
      this.sideToggled = false;
    }

    const event: Event = this.entries[this.editedIndex];
    event.timeline = this.timeline.id;

    let id: number;
    this.route.params.subscribe(data => id = data.id);

    this.entries[this.editedIndex] = await this.eventService.save(event, id);
  }

  /* Trigger d'un click sur le header d'une carte d'évènement */
  onHeaderClick(e) {
    if (this.stopPropagation) {
      e.stopPropagation();
      this.stopPropagation = false;
    }
    if (!this.expandEnabled) {
      e.stopPropagation();
    }
  }

  /* Trigger d'un click sur le point d'ancrage d'un évènement */
  onDotClick(e) {
    if (this.stopPropagation) {
      e.stopPropagation();
      this.stopPropagation = false;
    }
    if (!this.expandEnabled) {
      e.stopPropagation();
    }
  }

  selectEvent(expanded, index) {
    if (expanded) {
      this.expandedIndex = index;
      if (this.sideToggled) {
        this.editedEvent = this.entries[index];
      }
    } else if (this.expandedIndex === index) {
      this.expandedIndex = null;
      if (this.sideToggled) {
        this.closeSideBar();
      }
    }
  }

  /* Création d'un nouvel évènement */
  unshiftEntry($event) {
    this.entries.unshift($event);
  }

  /* Trigger du click sur le bouton d'Edition d'un event */
  edit(index) {
    // si l'event est déjà en edition, close la side bar
    if (this.expandedIndex === index) {
      if (this.editedIndex === index && this.sideToggled) {
        this.closeSideBar();
        return;
      }
      this.stopPropagation = true;
    }
    // si un autre event est déjà en edition, editer le nouvel event selectionner
    if (this.expandedIndex !== index && this.sideToggled) {
      this.editedEvent = this.entries[index];
      this.editedIndex = index;
    } else if (this.expandedIndex === index || !this.sideToggled) {
      this.openSideBar();
      this.editedEvent = this.entries[index];
      this.editedIndex = index;
    }
  }

  /* Trigger de l'event scroll down */
  onScrollDown() {
    const initialLength = this.entries.length;
    if (initialLength <= this.repoEntries.length - 20) {
      for (let i = initialLength; i < initialLength + 20; i++) {
        /* Utilisation du repository mocké */
        /* Sera remplacé par un call back-end pour query la page d'events suivantes */
        this.entries.push(this.repoEntries[i]);
      }
    }
  }

  toggleSide() {
    this.side = this.side === 'left' ? 'right' : 'left';
  }

  async editTimeline() {
    this.timelineEdit = !this.timelineEdit;

    if (!this.timelineEdit) {
      await this.timelineService.save(this.timeline);
    }
  }

  async removeAndDelete(index: number) {
    const event = this.entries[index];
    this.timeline.events = this.timeline.events.filter((evt: number) => evt !== event.id);
    this.entries = this.entries.filter((_: Event, i: number) => i !== index);
    await this.eventService.remove(event.id);
  }
}
