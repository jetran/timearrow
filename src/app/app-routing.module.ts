import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register';
import { LoginComponent } from './login';
import { HomeComponent } from './home';
import { TimelineFormComponent } from './timeline-form/timeline-form.component';
import { BrowseTimelinesComponent } from './browse-timelines/browse-timelines.component';
import {TimelineComponent} from './timeline/timeline.component';
import {ContributionViewerComponent} from './contribution-viewer/contribution-viewer.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'home', component: HomeComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'new/timeline', component: TimelineFormComponent},
  {path: 'timeline/:id', component: TimelineComponent},
  {path: 'timelines', component: BrowseTimelinesComponent},
  {path: 'contribution', component: ContributionViewerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
