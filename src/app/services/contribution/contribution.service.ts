import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Contribution} from '../../model/contribution';
import {Event} from '../../model/event';
import {JwtService} from '../jwt/jwt.service';
import {ActivatedRoute} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ContributionService {

  private contributionUri = 'http://127.0.0.1:82/contribution';

  constructor(private http: HttpClient, private jwtService: JwtService, private  route: ActivatedRoute) {
  }

  public async save(contribution: Contribution) {

    if (contribution.id) {
      return await this.post(contribution);
    } else {
      return await this.http.post<string>(this.contributionUri, JSON.stringify(contribution), {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
    }
  }

  public async fetchAll(): Promise<Contribution[]> {
    return await this.http.get<Contribution[]>(
      `${this.contributionUri}`,
      {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
  }

  public async fetch(id: number) {
    return await this.http.get<Contribution>(
      `${this.contributionUri}/${id}`,
      {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
  }

  private async post(contribution: Contribution) {
    return await this.http.put<string>(`${this.contributionUri}`, JSON.stringify(contribution), {
      headers:
        {
          authorization: `Bearer ${localStorage.getItem('access_token')}`
        }
    }).toPromise();
  }

  async add(before: Event, after: Event, id) {
    return await this.save(
      new Contribution(null, before.id, after.id, id, new Date(), this.jwtService.currentUser())
    );
  }

  async fetchByUser(id: number): Promise<Contribution[]> {
    return await this.http.get<Contribution[]>(
      `${this.contributionUri}/user/${id}`,
      {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
  }
}
