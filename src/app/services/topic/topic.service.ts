import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Topic} from '../../model/topic';

@Injectable({
  providedIn: 'root'
})
export class TopicService {

  private topicUri = 'http://127.0.0.1:86/topic';

  constructor(private http: HttpClient) {
  }

  public async save(topic: Topic): Promise<Topic> {

    if (topic.id) {
      return await this.put(topic);
    } else {
      return await this.http.post<Topic>(`${this.topicUri}`, JSON.stringify(topic), {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
    }
  }

  public async fetchAll(): Promise<Topic[]> {
    return await this.http.get<Topic[]>(
      `${this.topicUri}`,
      {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
  }

  public async fetch(id: number) {
    return await this.http.get<Topic>(
      `${this.topicUri}/${id}`,
      {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
  }

  private async put(topic: Topic) {
    return await this.http.put<Topic>(`${this.topicUri}/${topic.id}`, JSON.stringify(topic), {
      headers:
        {
          authorization: `Bearer ${localStorage.getItem('access_token')}`
        }
    }).toPromise();
  }

  async remove(id: number) {
    return await this.http.delete<Topic>(
      `${this.topicUri}/${id}`,
      {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
  }
}
