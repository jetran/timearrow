import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Timeline} from '../../model/timeline';
import {TopicService} from '../topic/topic.service';
import {DatePipe} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class TimelineService {

  private timelineUri = 'http://127.0.0.1:85/timeline';

  constructor(
    private http: HttpClient,
    private datePipe: DatePipe,
    private topicService: TopicService) {
  }

  public async save(timeline: Timeline) {

    if (timeline.id) {
      return await this.put(timeline);
    } else {

      // First : save the topics
      const listIdTopics = new Array();
      if (timeline.topics === null) {
        timeline.topics = [];
      } else {
        for (const topic of timeline.topics) {
          listIdTopics.push(topic);
        }
        // and add id in timeline array
        delete timeline.topics;
        timeline.topics = listIdTopics;
      }


      // Second : save the events and contributions : TODO
      if (timeline.events === null) {
        timeline.events = [];
      }

      // Finally : save timeline with topic, event, contributon's ids got previously
      const formattedTimeline = JSON.parse(JSON.stringify(timeline));
      formattedTimeline.from = this.datePipe.transform(timeline.from, 'yyyy-MM-dd');
      formattedTimeline.to = this.datePipe.transform(timeline.to, 'yyyy-MM-dd');

      return await this.http.post<Timeline>(this.timelineUri, JSON.stringify(formattedTimeline), {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
    }
  }

  public async fetchAll(): Promise<Timeline[]> {
    return await this.http.get<Timeline[]>(
      `${this.timelineUri}`,
      {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
  }

  public async fetch(id: number) {
    return await this.http.get<Timeline>(
      `${this.timelineUri}/${id}`,
      {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
  }

  private async put(timeline: Timeline) {
    return await this.http.put<Timeline>(`${this.timelineUri}/${timeline.id}`, JSON.stringify(timeline), {
      headers:
        {
          authorization: `Bearer ${localStorage.getItem('access_token')}`
        }
    }).toPromise();
  }

  async remove(id: number) {
    return await this.http.delete(
      `${this.timelineUri}/${id}`,
      {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
  }
}
