import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../../model/user';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userUri = 'http://127.0.0.1:86/user';

  constructor(private http: HttpClient) {
  }

  public async save(user: User) {

    if (user.id) {
      return await this.post(user);
    } else {
      return await this.http.post<string>(this.userUri, JSON.stringify(user), {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
    }
  }

  public async fetchAll(): Promise<User[]> {
    return await this.http.get<User[]>(
      `${this.userUri}`,
      {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
  }

  public async fetch(id: number) {
    return await this.http.get<User>(
      `${this.userUri}/${id}`,
      {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
  }

  private async post(user: User) {
    return await this.http.put<string>(`${this.userUri}`, JSON.stringify(user), {
      headers:
        {
          authorization: `Bearer ${localStorage.getItem('access_token')}`
        }
    }).toPromise();
  }
}
