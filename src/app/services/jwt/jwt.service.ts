import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import * as jwt from 'jsonwebtoken';

@Injectable({
  providedIn: 'root'
})

export class JwtService {

    private apiUrlUser = 'http://127.0.0.1:81/user';
    private apiUrlLogin = 'http://127.0.0.1:81/login';

    constructor(
      private httpClient: HttpClient,
      private router: Router
    ) { }

    register(email: string, username: string, password: string, firstname: string, lastname: string) {
      const parameter = JSON.stringify({username, password, email, firstName: firstname, lastName: lastname});

      return this.httpClient.post<{access_token: string}>
      (this.apiUrlUser,
        parameter);
    }

    login(email: string, password: string) {
      const parameter = JSON.stringify({email, password});
      const requestOptions: object = {
        responseType: 'text'
      };


      return this.httpClient.post<string>
      (this.apiUrlLogin,
        parameter,
        requestOptions
        )
        .pipe(first())
        .subscribe(
        data => {
          localStorage.setItem('access_token', data);
          this.router.navigate(['/']);
        });
    }

    logout() {
      localStorage.removeItem('access_token');
    }

    public get loggedIn(): boolean {
      return localStorage.getItem('access_token') !==  null;
    }

  currentUser() {
    const jwtPayload = jwt.verify(localStorage.getItem('access_token'), 'SeCrEt') as any;
    const {userId, email} = jwtPayload;
    return userId;
  }
}
