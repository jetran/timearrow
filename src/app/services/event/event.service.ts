import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Event} from '../../model/event';
import {ContributionService} from '../contribution/contribution.service';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  private eventUri = 'http://127.0.0.1:84/event';

  constructor(private http: HttpClient, private contributionService: ContributionService) {
  }

  public async save(event: Event, id: number) {
    const response = await this.http.post<Event>(this.eventUri, JSON.stringify(event), {
      headers:
        {
          authorization: `Bearer ${localStorage.getItem('access_token')}`
        }
    }).toPromise();
    this.contributionService.add(event, response, id);
    return response;
  }

  public async fetch(id: number) {
    return await this.http.get<Event>(
      `${this.eventUri}/${id}`,
      {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
  }

  public async fetchByTimeline(id: number) {
    return await this.http.get<Event[]>(
      `${this.eventUri}/timeline/${id}`,
      {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
  }

  private async put(event: Event) {

    return await this.http.put<Event>(`${this.eventUri}/${event.id}`, JSON.stringify(event), {
      headers:
        {
          authorization: `Bearer ${localStorage.getItem('access_token')}`
        }
    }).toPromise();
  }

  async remove(id: number) {
    return await this.http.delete(
      `${this.eventUri}/${id}`,
      {
        headers:
          {
            authorization: `Bearer ${localStorage.getItem('access_token')}`
          }
      }).toPromise();
  }
}
