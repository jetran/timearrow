import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {DiffResults} from 'ngx-text-diff/lib/ngx-text-diff.model';
import {Contribution} from '../model/contribution';
import {JwtService} from '../services/jwt/jwt.service';
import {ContributionService} from '../services/contribution/contribution.service';
import {TimelineService} from '../services/timeline/timeline.service';
import {EventService} from '../services/event/event.service';
import {Event} from '../model/event';

export interface DiffContent {
  leftContent: string;
  rightContent: string;
}

@Component({
  selector: 'app-contribution-viewer',
  templateUrl: './contribution-viewer.component.html',
  styleUrls: ['./contribution-viewer.component.scss']
})
export class ContributionViewerComponent implements OnInit {

  submitted = false;

  content: DiffContent = {
    leftContent: '',
    rightContent: ''
  };
  options: any = {
    lineNumbers: true,
    mode: 'xml'
  };

  contentObservable: Subject<DiffContent> = new Subject<DiffContent>();

  contentObservable$: Observable<DiffContent> = this.contentObservable.asObservable();
  contribution: Contribution;
  contributionList: any[] = [];
  displayedColumns: string[] = ['eventBefore', 'timeline', 'view'];

  constructor(
    private contributionService: ContributionService,
    private jwtService: JwtService,
    private timelineService: TimelineService,
    private eventService: EventService) { }

  async ngOnInit() {

    const contribution: any = {};
    const contributions = await this.contributionService.fetchByUser(this.jwtService.currentUser());
    let res = [];

    for (const c of contributions) {
      contribution.timeline = await this.timelineService.fetch(c.timeline);
      contribution.eventBefore = c.eventBefore ? await this.eventService.fetch(c.eventBefore) : new Event();
      contribution.eventAfter = c.eventAfter ? await this.eventService.fetch(c.eventAfter) : new Event();
      contribution.state = c.state;
      contribution.user = c.user;
      contribution.contribDate = c.contribDate;

      console.log(contribution);

      res.push(contribution);
    }

    this.contributionList = res;
  }

  buildContent(contribution: any) {
    // tslint:disable-next-line:max-line-length
    const fullLeftContent = `${contribution.eventBefore.title}\n${contribution.eventBefore.from ? contribution.eventBefore.from : ''}\n${contribution.eventBefore.to ? contribution.eventBefore.to : ''}\n${contribution.eventBefore.image}\n${contribution.eventBefore.video}\n${contribution.eventBefore.content}`;

    // tslint:disable-next-line:max-line-length
    const fullRightContent = `${contribution.eventAfter.title}\n${contribution.eventAfter.from ? contribution.eventAfter.from : ''}\n${contribution.eventAfter.to ? contribution.eventAfter.to : ''}\n${contribution.eventAfter.image}\n${contribution.eventAfter.video}\n${contribution.eventAfter.content}`;

    this.content.leftContent = fullLeftContent;
    this.content.rightContent = fullRightContent;

    console.log(fullLeftContent);

  }

  submitComparison(contribution: any) {
    this.submitted = false;
    this.contentObservable.next(this.content);
    this.buildContent(contribution);
    this.submitted = true;
  }

  handleChange(side: 'left' | 'right', value: string) {
    switch (side) {
      case 'left':
        this.content.leftContent = value;
        break;
      case 'right':
        this.content.rightContent = value;
        break;
      default:
        break;
    }
  }

  onCompareResults(diffResults: DiffResults) {
    console.log('diffResults', diffResults);
  }
}
