import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributionViewerComponent } from './contribution-viewer.component';

describe('ContributionViewerComponent', () => {
  let component: ContributionViewerComponent;
  let fixture: ComponentFixture<ContributionViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributionViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributionViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
