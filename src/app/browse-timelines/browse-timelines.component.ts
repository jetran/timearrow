import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {TimelineService} from '../services/timeline/timeline.service';
import {Timeline} from '../model/timeline';

@Component({
  selector: 'app-browse-timelines',
  templateUrl: './browse-timelines.component.html',
  styleUrls: ['./browse-timelines.component.scss']
})
export class BrowseTimelinesComponent implements OnInit {
  public timelines: Timeline[];
  displayedColumns: string[] = ['title', 'from', 'to', 'resume', 'view'];

  constructor(private router: Router, private timelineService: TimelineService) {
  }

  ngOnInit() {
    this.queryTimelines();
  }

  async queryTimelines() {
    this.timelines = await this.timelineService.fetchAll();
  }

  async deleteTimeline(id: number) {
    this.timelines = this.timelines.filter((timeline: Timeline) => timeline.id !== id);
    await this.timelineService.remove(id);
  }
}
